//
//  ViewController.swift
//  HitApp
//
//  Created by Alaa Maher on 4/11/20.
//  Copyright © 2020 Alaa Maher. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
	var books: [Book] = []

	@IBOutlet var tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Book List"
		loadBooks()
		        tableView.register(UINib(nibName: "BookTableViewCell", bundle: nil), forCellReuseIdentifier: "BookTableViewCell")
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}

	func loadBooks() {
		//	1
		guard let appDelegate =
			UIApplication.shared.delegate as? AppDelegate else {
				return
		}

		let persistentContainer = appDelegate.persistentContainer

		let managedContext =
			persistentContainer.viewContext

		managedContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
		//2
		let fetchRequest =
			NSFetchRequest<Book>(entityName: "Book")

		//3
		do {
			books = try managedContext.fetch(fetchRequest)
			for book in books {
				let author = book.authored_by?.name
				print(author)
			}
		} catch let error as NSError {
			print("Could not fetch. \(error), \(error.userInfo)")
		}
	}

	func loadAuthors() {
		//	1
		guard let appDelegate =
			UIApplication.shared.delegate as? AppDelegate else {
				return
		}

		let persistentContainer = appDelegate.persistentContainer

		let managedContext =
			persistentContainer.viewContext

		managedContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
		//2
		let fetchRequest =
			NSFetchRequest<Author>(entityName: "Author")

		//3
		do {
			let authors = try managedContext.fetch(fetchRequest)
			for author in authors {
				print(author.have?.allObjects)
			}
		} catch let error as NSError {
			print("Could not fetch. \(error), \(error.userInfo)")
		}

		do {
			try managedContext.save()
		} catch let error as NSError {
			print("Could not save. \(error), \(error.userInfo)")
		}

	}

		@IBAction func addName(_ sender: UIBarButtonItem) {
		let alert = UIAlertController(title: "New Book",
									  message: nil,
									  preferredStyle: .alert)

		let saveAction = UIAlertAction(title: "Save", style: .default) {
			[unowned self] action in

			guard let titleTextField = alert.textFields?.first,
				let authorTextField = alert.textFields?[1],
				let title = titleTextField.text, let author = authorTextField.text else {
					return
			}
			self.save(name: title, authorName: author)
			self.tableView.reloadData()
		}

		let cancelAction = UIAlertAction(title: "Cancel",
										 style: .cancel)

		alert.addTextField(
			configurationHandler: { (textField: UITextField) in
				textField.placeholder = "Title"
		})

		alert.addTextField(
			configurationHandler: { (textField: UITextField) in
				textField.placeholder = "Author"
		})


		alert.addAction(saveAction)
		alert.addAction(cancelAction)

		present(alert, animated: true)
	}

	func save(name: String, authorName: String) {
		guard let appDelegate =
			UIApplication.shared.delegate as? AppDelegate else {
				return
		}

		let persistentContainer = appDelegate.persistentContainer

		// 1
		let managedContext =
			persistentContainer.viewContext

		// 2
		let entity =
			NSEntityDescription.entity(forEntityName: "Book", in: managedContext)!
		let entityAuthor =
		NSEntityDescription.entity(forEntityName: "Author",
								   in: managedContext)!
		let book = Book(entity: entity, insertInto: managedContext)
//
		let author = Author(entity: entityAuthor, insertInto: managedContext)

		author.name = authorName
		author.addToHave(book)

		// 3
		book.name = name
		book.publication_year = "1921"

		// 4
		do {
			try managedContext.save()
			books.append(book)
		} catch let error as NSError {
			print("Could not save. \(error), \(error.userInfo)")
		}
	}

	func removeRecord(entry: String) {
		guard let appDelegate =
			UIApplication.shared.delegate as? AppDelegate else {
				return
		}

		let persistentContainer = appDelegate.persistentContainer

		let managedContext =
			persistentContainer.viewContext

		let request = NSFetchRequest<Book>(entityName: "Book")
		request.predicate = NSPredicate(format: "name = '\(entry)'")

		do {
			if let result = try? managedContext.fetch(request) {
				for object in result {
					managedContext.delete(object)
				}
			}
			self.books.removeAll(where: {$0.name == entry})
//			if let index = books.firstIndex(where: {$0.name == entry}) {
//				self.books.remove(at: index)
//			}
			try managedContext.save()
		} catch {
			print(error)
		}
		loadAuthors()
	}

	func update(name: String, newName: String) {
		guard let appDelegate =
			UIApplication.shared.delegate as? AppDelegate else {
				return
		}

		let managedContext =
			appDelegate.persistentContainer.viewContext

		let request = NSFetchRequest<Book>(entityName: "Book")
		request.predicate = NSPredicate(format: "name = '\(name)'")

		do {
			if let result = try? managedContext.fetch(request) {
				for object in result {
					object.setValue(newName, forKeyPath: "name")
				}
			}
			try managedContext.save()
		} catch {
			print(error)
		}
	}
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int {
		return books.count
	}

	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath)
		-> UITableViewCell {
			
						let book = books[indexPath.row]
			let cell =
				tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell",
											  for: indexPath) as! BookTableViewCell

			cell.setTitle(title: book.name!)
			cell.setAuthor(author: book.authored_by?.name ?? "No Author")
			return cell
	}


	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		let name = books[indexPath.row].name ?? ""
		self.removeRecord(entry: name)
		self.tableView.reloadData()
	}

	func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
		let name = books[indexPath.row].name ?? ""
		let update = UIContextualAction(style: .normal, title: "Update") { (action, view, completion ) in
			let alert = UIAlertController(title: "New Book",
										  message: nil,
										  preferredStyle: .alert)

			let saveAction = UIAlertAction(title: "Save", style: .default) {
				[unowned self] action in

				guard let textField = alert.textFields?.first,
					let nameToSave = textField.text else {
						return
				}

				self.update(name: name, newName: nameToSave)
				self.tableView.reloadData()
			}

			let cancelAction = UIAlertAction(title: "Cancel",
											 style: .cancel)

			alert.addTextField()

			alert.addAction(saveAction)
			alert.addAction(cancelAction)

			self.present(alert, animated: true)
			tableView.isEditing = false

		}

		let config = UISwipeActionsConfiguration(actions: [update])
		return config
	}
}
